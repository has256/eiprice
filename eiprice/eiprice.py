from flask import Flask, json
from pandas import pandas
from Products import Products

app = Flask(__name__)
products = Products()
# products.exporter()


@app.route('/search_by_ean/<ean>')
def search(ean):
    ean = int(ean)
    return json.dumps({'products': {
        f'{ean}': {'nome': products.name(ean),
                   'min_price': products.min_price(ean),
                   'max_price': products.max_price(ean),
                   'avg_price': products.avg_price(ean),
                   'mode_price': products.mode_price(ean),
                   'median_price': products.median_price(ean),
                   },
    }})


app.run()
