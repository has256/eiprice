import statistics
from Products import Products

products = Products()

ean_unit = 887961691313

unit = [
    119.9, 119.99, 124.99, 129.9, 139.99, 141.86, 143.99, 149.0, 149.9, 149.99, 150.0, 157.99, 159.49, 159.9, 169.9, 169.97, 169.99,
    173.0, 174.99, 179.88, 179.89, 179.95, 179.99, 189.9, 189.99, 196.9, 199.0, 199.99, 203.99, 204.89, 205.0, 207.0, 209.0, 218.0, 219.9, 219.99, 222.93, 229.99, 238.54]


def test_max(ean, unit):
    assert products.max_price(ean) == max(
        unit), 'Max price method must work'


def test_min(ean, unit):
    assert products.min_price(ean) == min(
        unit), 'Min price method must work'


def test_avg(ean, unit):
    assert products.avg_price(ean) == round(statistics.mean(
        unit), 2), 'Avg price method must work'


def test_median(ean, unit):
    assert products.median_price(ean) == round(statistics.median(
        unit), 2), 'Median price method must work'


def test_mode(ean, unit):
    assert products.mode_price(ean) == round(statistics.mode(
        unit), 2), 'Mode price method must work'


if __name__ == '__main__':
    test_max(ean_unit, unit)
    test_min(ean_unit, unit)
    test_avg(ean_unit, unit)
    test_median(ean_unit, unit)
    test_mode(ean_unit, unit)
