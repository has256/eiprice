import pandas as pd


class Products:

    def __init__(self, products_path='./config/products.csv'):
        self.__all_products = pd.read_csv(products_path, sep=';')

    def search(self, ean):
        product = self.__all_products[self.__all_products['ean'] == ean]
        return product

    def name(self, ean):
        name = self.search(ean)['produto'][0]
        return name

    def max_price(self, ean):
        prices = self.search(ean)['preco_por'].tolist()
        return max(prices)

    def min_price(self, ean):
        prices = self.search(ean)['preco_por'].tolist()
        return min(prices)

    def avg_price(self, ean):
        prices = self.search(ean)['preco_por'].tolist()
        return round(sum(prices) / len(prices), 2)

    def mode_price(self, ean):
        prices = self.search(ean)['preco_por'].tolist()
        prices_counter = []

        for i in range(len(prices)):
            prices_counter.append(prices.count(prices[i]))

        prices_frequency = list(zip(prices, prices_counter))

        mode_prices = [price for price,
                       freq in prices_frequency if freq == max(prices_counter)]

        return mode_prices if len(mode_prices) == 1 else mode_prices[0]

    def median_price(self, ean):
        prices = self.search(ean)['preco_por'].tolist()
        ordered_prices = sorted(prices)

        n = len(prices)
        even_quantity = n % 2 == 0

        first_median = ordered_prices[n // 2]
        second_median = ordered_prices[(n // 2) + 1]

        median_prices = first_median if even_quantity else (
            first_median + second_median) / 2

        return round(median_prices, 2)

    def exporter(self, data_path='./config/data.csv'):

        column_names = ['produto', 'ean', 'min_price',
                        'max_price', 'avg_price', 'mode_price', 'median_price']
        products_analysis = pd.DataFrame(columns=column_names)

        unique_products = self.__all_products.drop_duplicates(subset=["ean"])

        for _, product in unique_products.iterrows():

            products_analysis = products_analysis.append(
                {'produto': product['produto'],
                 'ean': product['ean'],
                 'min_price': self.min_price(product['ean']),
                 'max_price': self.max_price(product['ean']),
                 'avg_price': self.avg_price(product['ean']),
                 'mode_price': self.mode_price(product['ean']),
                 'median_price': self.median_price(product['ean']),
                 },
                ignore_index=True
            )

        products_analysis.to_csv(data_path)
        return products_analysis.to_dict()
