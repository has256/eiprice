# EIPrice

## How to run local

1. Certify that you have installed in your machine
	- [Git](https://git-for-windows.github.io/)
	- [Python 3.8 >](https://www.python.org/)
	- [Poetry](https://python-poetry.org/)

2. In your CLI, use the following commands!
   
```bash
# Clone this repository
$ git clone https://gitlab.com/has256/eiprice

# Go into the repository
$ cd eiprice

# Activate venv
$ poetry shell

# Install dependencies
$ poetry install --no-root

# Run the app
$ cd eiprice && python3 eiprice.py

# Go to http://localhost:5000/search_by_ean/887961691313 to test the app
```

## :hearts: Contributing

For bug fixes, documentation improvements, typos, translations (new/updates) and simple changes, just make a PR! tada
For more complex changes, pls, make an issue first so we can discuss the design.

Roadmap for contributing:

1. Fork it.
2. Create your feature branch (git checkout -b my-new-feature).
3. Commit your changes (git commit -am 'Add some feature').
4. Push to the branch (git push origin my-new-feature).
5. Create new Pull Request.

## :page_facing_up: License

Everything here is licensed by MIT License.